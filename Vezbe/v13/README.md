# Vežbe 13
*Cilj vežbi: Flask blueprint-ovi i JWT.*

## Zadaci
1. Prepraviti serversku aplikaciju tako da su nadležnosti za rad sa entitetima raspoređenje u nezavisne module.
2. Realizovati prijavu na sistem upotrebom JWT-a.
___
### Dodatne napomene:
* Dokumentacija za Flask blueprint-ove: https://flask.palletsprojects.com/en/2.1.x/tutorial/views/.
* Dokumentacija za JWT: https://jwt.io/
* Dokumentacija za Vue.js radni okvir: https://vuejs.org/guide/introduction.html
* Dokumentacija za Vue router: https://router.vuejs.org/
___
