import StudentiService from '../services/studenti-service.js';

export default {
    props: ["studenti"],
    emits: ["brisanje", "izmena"],
    methods: {
        ukloni(student) {
            StudentiService.delete(student.id).then(x=> {
                StudentiService.getAll().then(x=>{
                    x.json().then(v=>{
                        this.podaciStudenata = v;
                    })
                })
            });
        },
        izmeni(student) {
            this.$router.push(`/studentForma/${student.id}`)
        }
    },
    data() {
        return {
            podaciStudenata: []
        }
    },
    watch: {
        studenti: function(novaVrednost, staraVrednost) {
            this.podaciStudenata = novaVrednost;
        }
    },
    created() {
        StudentiService.getAll().then(response =>{
            response.json().then(v=> {
                this.podaciStudenata = v;
            })
        })
    },
    template: `
    <table>
    <thead>
        <tr>
            <th>Ime</th>
            <th>Prezime</th>
            <th>Smer</th>
            <th>Prosečna ocena</th>
            <th>Akcije</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="(student, i) in podaciStudenata">
            <td>{{student.ime}}</td>
            <td>{{student.prezime}}</td>
            <td>{{student.smer.sifra}} - {{student.smer.naziv}}</td>
            <td>{{student.prosecna_ocena}}</td>
            <td>
                <button @click="ukloni(student)">Ukloni</button>
                <button @click="izmeni(student)">Izmeni</button>
                <router-link :to="'/studentForma/' + student.id">Izmeni</router-link>
            </td>
        </tr>
    </tbody>
</table>
    `
}