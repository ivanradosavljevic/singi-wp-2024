import StudentiService from '../services/studenti-service.js';
import SmeroviService from '../services/smerovi-service.js';

export default {
    props: ["student"],
    emits: ["sacuvaj"],
    methods: {
        sacuvaj() {
            if(this.noviStudent.id != undefined) {
                StudentiService.update(this.noviStudent).then(x => {
                    this.$router.push("/");
                });
            } else {
                StudentiService.create(this.noviStudent).then(x=>{
                    this.$router.push("/");
                });
            }
        },
        ponistiFormu() {
            this.noviStudent = {};
        }
    },
    watch: {
        student(novaVrednost, staraVrednost) {
            this.noviStudent = novaVrednost;
        }
    },
    data() {
        return {
            noviStudent: {},
            smerovi: []
        }
    },
    created() {
        if(this.$route.params.idStudenta) {
            StudentiService.getById(this.$route.params.idStudenta).then(r => {
                if (r.status == 200) {
                    r.json().then(podaci => {
                        this.noviStudent = podaci;
                    })
                }
            });
        }
        SmeroviService.getAll().then(r => {
            if (r.status == 200) {
                r.json().then(podaci => {
                    this.smerovi = podaci;
                })
            }
        })
    },
    template: `
    <form v-on:submit.prevent="sacuvaj()" action="">
    <label>Broj indeksa: <input v-model="noviStudent.broj_indeksa" type="text" required></label>
    <label>Ime: <input v-model="noviStudent.ime" type="text" required></label>
    <label>Prezime: <input v-model="noviStudent.prezime" type="text" required></label>
    <label>Smer:
        <select v-model="noviStudent.studijski_program" required>
            <option v-for="smer in smerovi" :value="smer.id">{{smer.naziv}}</option>
        </select>
    </label>
    <label>Prosečna ocena: <input v-model="noviStudent.prosecna_ocena" type="number" min="5" max="10"
            step="0.01" required></label>

    <button v-if="noviStudent.id != undefined" type="submit">Izmeni</button>
    <button v-if="noviStudent.id != undefined" type="reset" @click="ponistiFormu()">Odustani</button>
    <button v-else type="submit">Dodaj</button>
</form>
    `
}