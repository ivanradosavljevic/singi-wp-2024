import PrimerKomponente from "./components/primer-komponente.js"
import Tabela from "./components/tabela.js"
import Forma from "./components/forma.js"

import StudentiService from './services/studenti-service.js';

fetch("/api/studenti").then(response => {
    let json = response.json().then(vrednosti => {
        console.log(vrednosti);
    });
});
const { createApp } = Vue

const app = createApp(
    {
        data() {
            return {
            }
        },
        created() {
            StudentiService.primer().then(r => {
                console.log(r);
            }, reason=> {
                console.log("Promis neuspesno razresen: " + reason);
            });
        },
        methods: {
        }
    }
);

app.use(VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes: [
        { path: "/", component: Tabela },
        { path: "/studentForma", component: Forma },
        { path: "/studentForma/:idStudenta", component: Forma }
    ]
}));

app.component("primer-komponente", PrimerKomponente);
app.component("tabela", Tabela);
app.component("forma", Forma);
app.mount('#app');

console.log(app);