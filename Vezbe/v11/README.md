# Vežbe 11
*Cilj vežbi: Rutiranje u klijentskoj aplikaciji.*

## Zadaci
1. Prepraviti zadatak 10 tako da se u aplikaciji može prelaziti sa stranice na stranicu. Svaka stranica treba da prikazuje po jednu komponentu. Obezbediti stranice za prikaz forme i prikaz tabele.
___
### Dodatne napomene:
* Dokumentacija za Vue.js radni okvir: https://vuejs.org/guide/introduction.html
* Dokumentacija za Vue router: https://router.vuejs.org/
___
