export default {
    props: ["studenti"],
    emits: ["brisanje", "izmena"],
    methods: {
        ukloni(student) {
            this.$emit("brisanje", {...student});
        },
        izmeni(student) {
            this.$emit("izmena", {...student});
        }
    },
    template: `
    <table>
    <thead>
        <tr>
            <th>Ime</th>
            <th>Prezime</th>
            <th>Smer</th>
            <th>Prosečna ocena</th>
            <th>Akcije</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="(student, i) in studenti">
            <td>{{student.ime}}</td>
            <td>{{student.prezime}}</td>
            <td>{{student.smer.sifra}} - {{student.smer.naziv}}</td>
            <td>{{student.prosecna_ocena}}</td>
            <td>
                <button @click="ukloni(student)">Ukloni</button>
                <button @click="izmeni(student)">Izmeni</button>
            </td>
        </tr>
    </tbody>
</table>
    `
}