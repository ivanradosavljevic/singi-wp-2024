# Vežbe 2
*Cilj vežbi: Generisanje sadržaja na serverskoj strani.*

## Zadaci
1. Napraviti web aplikaciju za upravljanje podacima o studentima. Svaki student je opisan podacima: broj indeksa, ime, prezime i prosečna ocena.
2. Napraviti početnu stranicu na kojoj se prikazuju svi studenti registrovani u aplikaciji.
3. Napraviti stranicu za dodavanje studenata u aplikaciju.
4. Omogućiti uklanjanje studenata koji su dodati.
5. Napraviti stranicu za izmenu studenata dodatih u aplikaciji.
___
### Dodatne napomene:
* Dokumentacija za Flask radni okvir: https://palletsprojects.com/p/flask/.
___